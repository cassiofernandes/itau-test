$(document).ready(function () {
  var dadosFatura = [
    {
      nome: "Uber",
      valor: "1430.00",
      mes: 05,
    },
    {
      nome: "PS Store",
      valor: "40.00",
      mes: 05,
    },
    {
      nome: "GPlay",
      valor: "37.50",
      mes: 03,
    },
    {
      nome: "Uber",
      valor: "1382.00",
      mes: 03,
    },
    {
      nome: "iFood",
      valor: "77.00",
      mes: 02,
    },
    {
      nome: "Uber",
      valor: "3.00",
      mes: 01,
    },
    {
      nome: "iFood",
      valor: "20.34",
      mes: 01,
    },
    {
      nome: "iFood",
      valor: "37.40",
      mes: 01,
    },
    {
      nome: "Uber",
      valor: "17.70",
      mes: 03,
    },
    {
      nome: "GPlay",
      valor: "16.70",
      mes: 03,
    },
    {
      nome: "AppStore",
      valor: "15.00",
      mes: 02,
    },
    {
      nome: "Uber",
      valor: "96.11",
      mes: 02,
    },
    {
      nome: "GPlay",
      valor: "7.71",
      mes: 02,
    },
    {
      nome: "AppStore",
      valor: "3.33",
      mes: 02,
    },
    {
      nome: "iFood",
      valor: "47.98",
      mes: 03,
    },
    {
      nome: "AppStore",
      valor: "12.22",
      mes: 03,
    },
    {
      nome: "Uber",
      valor: "2.56",
      mes: 04,
    },
    {
      nome: "Uber",
      valor: "5.32",
      mes: 03,
    },
    {
      nome: "PS Store",
      valor: "5.44",
      mes: 03,
    },
    {
      nome: "PS Store",
      valor: "98.37",
      mes: 03,
    },
    {
      nome: "PS Store",
      valor: "78.90",
      mes: 01,
    },
    {
      nome: "GPlay",
      valor: "65.03",
      mes: 03,
    },
    {
      nome: "iFood",
      valor: "32.12",
      mes: 03,
    },
    {
      nome: "Uber",
      valor: "34.56",
      mes: 01,
    },
    {
      nome: "Uber",
      valor: "34.56",
      mes: 01,
    },
    {
      nome: "iFood",
      valor: "1480.00",
      mes: 01,
    },
    {
      nome: "Uber",
      valor: "5.34",
      mes: 03,
    },
    {
      nome: "iFood",
      valor: "6.12",
      mes: 01,
    },
  ];
  //var um = 0;

  let total = 0; // Variável auxiliar
  let totalFev = 0; // Variável auxiliar
  let totalMar = 0;
  // Itera sobre a lista de elementos tds (tdsValores)

  dadosFatura.sort(function (a, b) {
    return a.mes < b.mes ? -1 : a.mes > b.mes ? 1 : 0;
  });

  var jan;
  var mess;
  var fev;
  var mar;

  for (let ii = 0; ii < dadosFatura.length; ii++) {
    let valor = parseFloat(dadosFatura[ii].valor);

    if (dadosFatura[ii].mes == 01) {
      total = total + valor;
      jan = dadosFatura[ii].mes = "Janeiro";
    }
    if (dadosFatura[ii].mes == 02) {
      totalFev = totalFev + valor;
      fev = dadosFatura[ii].mes = "Fevereiro";
    }
    if (dadosFatura[ii].mes == 03) {
      totalMar = total + valor;
      mar = dadosFatura[ii].mes = "Março";
    }
  }

  $("#jan").append(
    '<li class="janeiro"><span class="negrito">' +
      jan +
      "</span> | R$ " +
      total.toFixed(2) +
      '</li><li class="fevereiro"><span class="negrito">' +
      fev +
      "</span> | R$ " +
      totalFev.toFixed(2) +
      '</li><li class="fevereiro"><span class="negrito">' +
      mar +
      "</span> | R$ " +
      totalMar.toFixed(2) +
      "</li>"
  );

  for (let i = 0; i < dadosFatura.length; i++) {
    if (dadosFatura[i].mes == 01) {
      dadosFatura[i].mes = "Janeiro";
    } else if (dadosFatura[i].mes == 02) {
      dadosFatura[i].mes = "Fevereiro";
    } else if (dadosFatura[i].mes == 03) {
      dadosFatura[i].mes = "Março";
    } else if (dadosFatura[i].mes == 04) {
      dadosFatura[i].mes = "Abril";
    } else if (dadosFatura[i].mes == 05) {
      dadosFatura[i].mes = "Maio";
    }

    $("tbody").append(
      "<tr><td>" +
        dadosFatura[i].nome +
        "</td><td> R$ " +
        dadosFatura[i].valor +
        "</td><td>" +
        dadosFatura[i].mes +
        "</td></tr>"
    );
  }

  console.log(total); // imprime no console o valor final de total
});
